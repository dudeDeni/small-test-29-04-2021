const sum = require("./sum.js");


describe("sum.js testing", () => {
    test("1+2 equal 3", () => {
        const value = sum(1,2);
        expect(value).toBe(3);
    });
    
    test("8+2 equal 10", () => {
        // TODO: !!!
        const value = sum(8,2);
        expect(value).toBe(10);
    });

    test("3+8 does not equal 10", () => {
        // TODO: !!!
        const value = sum(3,8);
        expect(value).toBe(11);
    });
});
