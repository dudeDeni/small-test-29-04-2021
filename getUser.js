const faker = require("faker");
function getUser(userID) {
    faker.seed(userID);
    
    return {
        id: userID,
        name: `${faker.name.firstName()} ${faker.name.lastName()}`,
        email: faker.internet.email(),
        city: faker.address.city(),
        company: faker.company.companyName(),
    };
}
module.exports = getUser;